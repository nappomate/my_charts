var fileName = "PerformAnalyse_2017-05-17_11-59-16.json";
var totalSizeNameString = "Benötigte Zeit [ms]: ";
var einheitenString = " ms";
var partitionsvariable;
var performanceJSON;  //globale Variable, damit sie auch nach der createNewPerformanceJSON abrufbar ist

/**Zu Begin starten mit eingetragenem JSON */
newJSONinput();


/**  Einlesen der neuen JSON-Datei  */
function readNewFile(input) {
        fileName = input;
        newJSONinput();
}


function newJSONinput()
{
    /** Clear all old entries */
    document.getElementById("performanceTotalDiv").innerHTML = "";
    document.getElementById("performanceThreadDiv").innerHTML = "";
    document.getElementById("performanceClassDiv").innerHTML = "";
    //delete old SVG
    d3.select("svg").remove();




    /**Unsere JSON einlesen*/
    // d3.json(fileName, function (data) {
    //     writePhoneInfo(data.phoneInformation);
    //     createNewPerformanceJson(data.performanceMap);
    // });
    $.getJSON(fileName, function (data) {
        writePhoneInfo(data.phoneInformation);
        createNewPerformanceJson(data.performanceMap);
    });
}

/** PhoneInformation aus JSON auslesen und in <div> schreiben */
function writePhoneInfo(phoneInfo) {

    document.getElementById("phoneInfo").innerHTML = ""; //clear old text
    d3.select("#phoneInfo")
        .append("p")
        .html
        (
            "<b>"+"Telefon Informationen: " + "</b>"+
            "totalMemoryInMB: " + phoneInfo.totalMemoryInMB +"<br>" +
            "usedMemoryInMB: " + phoneInfo.usedMemoryInMB +"<br>" +
            "availableMemoryInMB: "+ phoneInfo.availableMemoryInMB + "<br>" +
            "board: " + phoneInfo.board + "<br>" +
            "brand: " +phoneInfo.brand +"<br>" +
            "device: " + phoneInfo.device +"<br>" +
            "hardware: "+ phoneInfo.hardware +"<br>" +
            "manufacturer: " + phoneInfo.manufacturer +"<br>" +
            "model: " +  phoneInfo.model +"<br>" +
            "versionCodeName: " + phoneInfo.versionCodeName +"<br>" +
            "versionRelease: " + phoneInfo.versionRelease
        );
}


/**

 * @param performanceMap
 * Erstelle aus dem PerformanceMap Teil der alten JSON neues Object, was mit der Diagramm Notation übereinstimmt
 */
function createNewPerformanceJson(performanceMap)
{
    var newJson = {
        name : "ThreadId",
        children : []
    };

    for (var threadId in performanceMap)
    {
        var threadEbene = {};
        threadEbene["name"] = threadId;

        var threadChildren = [];
        for (var className in performanceMap[threadId])
        {
            var classEbene = {
                name : className,
                children : []
            };
            var classChildren = [];
            for (var methodName in performanceMap[threadId][className])
            {
                var methodEbene = {};
                methodEbene["name"]=methodName;
                methodEbene["count"]=performanceMap[threadId][className][methodName].count;
                methodEbene["maxNeededTime"]=performanceMap[threadId][className][methodName].maxNeededTime;
                methodEbene["overallNeededTime"]=performanceMap[threadId][className][methodName].overallNeededTime;
                var averageTime = (performanceMap[threadId][className][methodName].overallNeededTime/performanceMap[threadId][className][methodName].count).toFixed(2);
                methodEbene["averageNeededTime"]= averageTime;

                // console.log("Name: "+methodName+ ",    averageTime:" +a);
                classChildren.push(methodEbene);
            }
            classEbene["children"] = classChildren;
            threadChildren.push(classEbene);
        }
        threadEbene["children"] = threadChildren;
        newJson.children.push( threadEbene );
    }

    performanceJSON=newJson;  //setze neu erstellte JSON auf globale Variable





}


function radioButtonPressed() {
    if(document.getElementById('count').checked) {
        partitionsvariable = 'count';
        totalSizeNameString = "Anzahl der Aufrufe: ";
        einheitenString = " Aufrufe";

        allJson();
    }
    if(document.getElementById('max').checked) {
        partitionsvariable = 'maxNeededTime';
        totalSizeNameString = "Benötigte Zeit [ms]: ";
        einheitenString = " ms";
        allJson();
    }
    if(document.getElementById('overall').checked) {
        partitionsvariable = 'overallNeededTime';
        totalSizeNameString = "Benötigte Zeit [ms]: ";
        einheitenString = " ms";
        allJson();
    }
    if(document.getElementById('average').checked) {
        partitionsvariable = 'averageNeededTime';
        totalSizeNameString = "Benötigte Zeit [ms]: ";
        einheitenString = " ms";
        allJson();
    }
}


var width = 820,
    height = 720,
    radius = (Math.min(width, height)-60) / 2;


function allJson(){


    var totalDiv = d3.select("#performanceTotalDiv");
    var threadDiv = d3.select("#performanceThreadDiv");
    var classDiv = d3.select("#performanceClassDiv");
    var methodDiv = d3.select("#performanceMethodDiv");

    //delete old SVG
    d3.select("svg").remove();

    var x = d3.scale.linear()
        .range([0, 2 * Math.PI]);

    var y = d3.scale.linear()
        .range([0, radius]);

    var color = d3.scale.category20c();


    partition = d3.layout.partition()
        .value(function(d) {
            return d[partitionsvariable]; });


    var svg = d3.select("#svgDiv").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2.2 + "," + (height / 2 + 10) + ")");



    var arc = d3.svg.arc()
        .startAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
        .endAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
        .innerRadius(function(d) { return Math.max(0, y(d.y)); })
        .outerRadius(function(d) { return Math.max(0, y(d.y + d.dy)); });


    var g = svg.selectAll("g")
        .data(partition.nodes(JSON.parse(JSON.stringify(performanceJSON))))
        .enter().append("g");





    //delete TooltipDiv from old SVG
    var oldTooltip = document.getElementById("tooltipDiv");
    if (oldTooltip) { //überprüfe, ob altes Div existiert
        oldTooltip.parentNode.removeChild(oldTooltip); //löschen
    }

    // Define the div for the tooltip
    var diva = d3.select("#svgDiv").append("div")
        .attr("id","tooltipDiv")
        .attr("class", "tooltip")
        .style("opacity", 0);


    var path = g.append("path")
        .attr("d", arc)
        .style("fill", function(d) { return color((d.children ? d : d.parent).name); })
        .on("click", SunburstClick)
        .on("mouseover", mouseOverBar)
        .on("mouseout", function(d) {
            diva.transition()
                .duration(500)
                .style("opacity", 0);
        });

    function mouseOverBar (d){

        if (d.depth <=2 ) //Maus über Class/Thread/Root
        {
            diva.transition()
            .duration(200)
            .style("opacity", .99);
            if (partitionsvariable == "averageNeededTime"){
                diva.html(d.name + "<br/>"  + d.value + einheitenString)
                    .style("left", (d3.event.pageX) + "px")
                    .style("height", 30+"px")
                    .style("top", (d3.event.pageY - 28) + "px");
            }
            else {
                diva.html(d.name + "<br/>"  + d.value + einheitenString)
                    .style("left", (d3.event.pageX) + "px")
                    .style("height", 30+"px")
                    .style("top", (d3.event.pageY - 28) + "px");
            }


        }
         else //Maus über Methodenebene
             {
            diva.transition()
                .duration(200)
                .style("opacity", .99);
            diva.html(d.name + "<br/>"  +
                d.count +" Aufrufe"+ "<br>"+
                "MaxTime: "+d.maxNeededTime +  "ms"+"<br>"+
                    "Average: "+d.averageNeededTime + " ms <br>"+
                    "Overall: "+d.overallNeededTime+ " ms"
            )
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px")
                .style("height", "auto")
                .style("width", "auto")
            ;
        }
    }
    totalSize = path.datum().value;

    //Runde die Durchschnittszeiten auf 2 Nachkommastellen
    if (partitionsvariable == "averageNeededTime") {
        // totalSize = totalSize.toFixed(2);
        totalSize = Math.round(totalSize*100)/100;
    }
    document.getElementById("performanceTotalDiv").innerHTML = ""; //clear old text
    document.getElementById("performanceThreadDiv").innerHTML = ""; //leere ThreadDiv
    document.getElementById("performanceClassDiv").innerHTML = ""; //leere ClassDiv

    //Einfügen in die totalDiv
    totalDiv
        .append("p")
        .html
        (
            "<b>All: </b><br>"+
            totalSizeNameString + totalSize
        );

    var text = g.append("text")
        .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
        .attr("x", function(d) { return y(d.y); })
        .attr("dx", "6") // margin
        .attr("dy", ".35em") // vertical-align
        .text(function(d) { return d.name; });


    function SunburstClick(d){

        /*Anklicken des Root-Knotens*/
        if (d.depth == 0){
            document.getElementById("performanceThreadDiv").innerHTML = ""; //leere ThreadDiv
            document.getElementById("performanceClassDiv").innerHTML = ""; //leere ClassDiv
            document.getElementById("performanceMethodDiv").innerHTML = ""; //leere MethodDiv
        }

        //Anklicken der Threadebene
        if (d.depth == 1){

            /**Leeren aller Unterfelder */
            document.getElementById("performanceThreadDiv").innerHTML = ""; //clear old text
            document.getElementById("performanceClassDiv").innerHTML = ""; //leere ClassDiv
            document.getElementById("performanceMethodDiv").innerHTML = ""; //leere MethodDiv

            /**Einfügen der Thread-ID in das vorgesehene <div> */
            threadDiv.append("p")
                .html
                (
                    "<b>"+d.name+"</b><br>"+
                    totalSizeNameString+ d.value
                );




            /**Einfügen der enthaltenen Klassen*/
            classDiv.style("height", "600px")  //Festsetzen der <div>, damit Scrolloption aktiv wird
                .append("p")
                .html
                ("<b> Enthaltene Klassen: </b>"
                );



            $("#performanceClassDiv")
                .append("<ul class='list-group'>");  //Beginnen der GroupList
            var classEntryColor;

            /**Laufe durch alle im Thread enthaltenen Klassen */
            for (var klassen in d.children){

                var timeValue = d.children[klassen].value;

                if ((partitionsvariable == "maxNeededTime") || (partitionsvariable == "averageNeededTime")){

                    timeValue = 1;
                    if (partitionsvariable == "averageNeededTime") {
                        for (var methoden in d.children[klassen].children) {
                            var currentMethod = d.children[klassen].children[methoden];
                            timeValue = parseInt(timeValue);
                            if (timeValue < currentMethod.averageNeededTime) {
                                timeValue = currentMethod.averageNeededTime;
                            }
                        }
                    }
                    if (partitionsvariable == "maxNeededTime"){
                        for (var methoden in d.children[klassen].children) {
                            var currentMethod = d.children[klassen].children[methoden];
                            timeValue = parseInt(timeValue);
                            if (timeValue < currentMethod.maxNeededTime) {
                                timeValue = currentMethod.maxNeededTime;
                            }
                        }
                    }

                    if (timeValue < 20){
                        classEntryColor="badge badge-default";
                    }
                    else {
                        classEntryColor="badge badge-danger";
                    }
                }
                else {
                    classEntryColor="badge badge-default";

                }
                var klassenInformationen  = $("<li class='list-group-item link'><span class='"+classEntryColor+"'>"+timeValue+einheitenString+"</span>" + d.children[klassen].name+".class  "+"</li>");

                klassenInformationen.on("click", {child: d.children[klassen]}, function(e) {
                    SunburstClick(e.data.child);
                });

                $("#performanceClassDiv")
                    .append(klassenInformationen);

            }
            $("#performanceClassDiv")
                .append("</ul>");    //abschließen der Grouplist


        }



        //Anklicken der Klassenebene
        if (d.depth == 2){

            /**Einfügen der ThreadInformationen*/
            if (document.getElementById("performanceThreadDiv").innerHTML == "")//Überprüfen ob Threadinformationen bereits eingetragen wurden
            {

                threadDiv
                    .append("p")
                    .html
                    (
                        "<b>"+d.parent.name+"</b><br>"+
                        totalSizeNameString+ d.parent.value
                    );
            }

            /**Einfügen der KlassenInformationen*/
            document.getElementById("performanceClassDiv").innerHTML = ""; //clear old text
            classDiv
                .style("height", "auto")
                .append("p")
                .html
                (
                    "<b>Klasse: </b>"+d.name+"<br>"+
                    totalSizeNameString+ d.value
                );


            /**Einfügen der MethodenInformationen*/
            document.getElementById("performanceMethodDiv").innerHTML = ""; //clear old text
            methodDiv
                .append("p")
                .html
                ("<b> Enthaltene Methoden: </b>"
                );
            for (var methoden in d.children)
            {
                methodDiv
                    .append("p")
                    .html
                    (d.children[methoden].name + ": <br> "+ d.children[methoden][partitionsvariable] +einheitenString
                    );

            }
        }


        //Anklicken der Methodenebene
        if (d.depth == 3){

            /**Einfügen der ThreadInformationen*/
            if (document.getElementById("performanceThreadDiv").innerHTML == "") //Überprüfen ob Threadinformationen bereits eingetragen wurden
            {
                threadDiv
                    .append("p")
                    .html
                    (
                        "<b>"+d.parent.parent.name+"</b><br>"+
                        totalSizeNameString+ d.parent.parent.value
                    );
            }


            /**Einfügen der KlassenInformationen*/

            document.getElementById("performanceClassDiv").innerHTML = "";
            classDiv
                .style("height","auto")
                .append("p")
                .html
                (
                    "<b>Klasse: </b>" + d.parent.name + "<br>" +
                    totalSizeNameString + d.parent.value
                );


            /**Einfügen der detailierten MethodenInformationen*/
            document.getElementById("performanceMethodDiv").innerHTML = ""; //clear old text

            methodDiv
                .append("p")
                .html
                ("<b>Methode: </b>"+d.name + "<br>"+
                        d.count + " Aufruf(e)" +"<br>"+
                        "MaxTime: "+d.maxNeededTime+ " ms"+"<br>"+
                        "AverageTime: "+d.averageNeededTime+ " ms"+"<br>"+
                        "OverallTime: "+d.overallNeededTime+ " ms"
                );

        }


        // fade out all text elements

        text.transition().attr("opacity", 0);

        path.transition()
            .duration(750)
            .attrTween("d", arcTween(d))
            .each("end", function(e, i) {

                log(e);
                // check if the animated element's data e lies within the visible angle span given in d
                if (e.x >= d.x && e.x < (d.x + d.dx)) {
                    // get a selection of the associated text element
                    var arcText = d3.select(this.parentNode).select("text");
                    // fade in the text element and recalculate positions
                    arcText.transition().duration(750)
                        .attr("opacity", 1)
                        .attr("transform", function() { return "rotate(" + computeTextRotation(e) + ")" })
                        .attr("x", function(d) { return y(d.y); });
                }
            });
    }



    d3.select(self.frameElement).style("height", height + "px");

// Interpolate the scales!
    function arcTween(d) {
        var xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
            yd = d3.interpolate(y.domain(), [d.y, 1]),
            yr = d3.interpolate(y.range(), [d.y ? 20 : 0, radius]);
        return function(d, i) {
            return i
                ? function(t) { return arc(d); }
                : function(t) { x.domain(xd(t)); y.domain(yd(t)).range(yr(t)); return arc(d); };
        };
    }

    function computeTextRotation(d) {
        return (x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
    }

}


