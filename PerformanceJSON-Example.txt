{
  "name": "ThreadId",
  "children": [
    {
      "name": "MainThread",
      "children": [
        {
          "name": "",
          "children": [
            {
              "name": "onCheckedChanged",
              "count": 1,
              "maxNeededTime": 1,
              "overallNeededTime": 1,
              "averageNeededTime": "1.00"
            },
            {
              "name": "onClick",
              "count": 11,
              "maxNeededTime": 41,
              "overallNeededTime": 200,
              "averageNeededTime": "18.18"
            },
            {
              "name": "onPageSelected",
              "count": 2,
              "maxNeededTime": 8,
              "overallNeededTime": 16,
              "averageNeededTime": "8.00"
            },
            {
              "name": "onTabSelected",
              "count": 2,
              "maxNeededTime": 11,
              "overallNeededTime": 12,
              "averageNeededTime": "6.00"
            },
            {
              "name": "run",
              "count": 1,
              "maxNeededTime": 79,
              "overallNeededTime": 79,
              "averageNeededTime": "79.00"
            }
          ]
        },
        {
          "name": "AddFavorite",
          "children": [
            {
              "name": "Addfavorite",
              "count": 3,
              "maxNeededTime": 17,
              "overallNeededTime": 48,
              "averageNeededTime": "16.00"
            }
          ]
        },
        {
          "name": "BaseActivitiy",
          "children": [
            {
              "name": "onStart",
              "count": 2,
              "maxNeededTime": 3,
              "overallNeededTime": 4,
              "averageNeededTime": "2.00"
            }
          ]
        },
        {
          "name": "FavoriteRecyclerAdapter",
          "children": [
            {
              "name": "getItemCount",
              "count": 1,
              "maxNeededTime": 1,
              "overallNeededTime": 1,
              "averageNeededTime": "1.00"
            },
            {
              "name": "onBindViewHolder",
              "count": 24,
              "maxNeededTime": 13,
              "overallNeededTime": 36,
              "averageNeededTime": "1.50"
            },
            {
              "name": "onCreateViewHolder",
              "count": 35,
              "maxNeededTime": 7,
              "overallNeededTime": 83,
              "averageNeededTime": "2.37"
            }
          ]
        },
        {
          "name": "FavoritenFragment",
          "children": [
            {
              "name": "getFavoriteList",
              "count": 10,
              "maxNeededTime": 7,
              "overallNeededTime": 34,
              "averageNeededTime": "3.40"
            },
            {
              "name": "onActivityCreated",
              "count": 1,
              "maxNeededTime": 6,
              "overallNeededTime": 6,
              "averageNeededTime": "6.00"
            },
            {
              "name": "onCreateView",
              "count": 1,
              "maxNeededTime": 7,
              "overallNeededTime": 7,
              "averageNeededTime": "7.00"
            },
            {
              "name": "onResume",
              "count": 7,
              "maxNeededTime": 4,
              "overallNeededTime": 19,
              "averageNeededTime": "2.71"
            },
            {
              "name": "refreshView",
              "count": 9,
              "maxNeededTime": 8,
              "overallNeededTime": 35,
              "averageNeededTime": "3.89"
            }
          ]
        },
        {
          "name": "FilterActivity",
          "children": [
            {
              "name": "onCreate",
              "count": 1,
              "maxNeededTime": 12,
              "overallNeededTime": 12,
              "averageNeededTime": "12.00"
            },
            {
              "name": "onOptionsItemSelected",
              "count": 3,
              "maxNeededTime": 267,
              "overallNeededTime": 765,
              "averageNeededTime": "255.00"
            },
            {
              "name": "onThumbnailClick",
              "count": 5,
              "maxNeededTime": 40,
              "overallNeededTime": 130,
              "averageNeededTime": "26.00"
            }
          ]
        },
        {
          "name": "FlickrTask",
          "children": [
            {
              "name": "onPostExecute",
              "count": 2,
              "maxNeededTime": 5,
              "overallNeededTime": 6,
              "averageNeededTime": "3.00"
            }
          ]
        },
        {
          "name": "FullscreenImageFromFavoritenActiviy",
          "children": [
            {
              "name": "onActivityResult",
              "count": 1,
              "maxNeededTime": 29,
              "overallNeededTime": 29,
              "averageNeededTime": "29.00"
            },
            {
              "name": "onCreate",
              "count": 4,
              "maxNeededTime": 10,
              "overallNeededTime": 39,
              "averageNeededTime": "9.75"
            },
            {
              "name": "onCreateOptionsMenu",
              "count": 4,
              "maxNeededTime": 7,
              "overallNeededTime": 19,
              "averageNeededTime": "4.75"
            },
            {
              "name": "onOptionsItemSelected",
              "count": 3,
              "maxNeededTime": 19,
              "overallNeededTime": 45,
              "averageNeededTime": "15.00"
            }
          ]
        },
        {
          "name": "FullscreenImageFromSearchActivity",
          "children": [
            {
              "name": "onCreate",
              "count": 2,
              "maxNeededTime": 10,
              "overallNeededTime": 19,
              "averageNeededTime": "9.50"
            },
            {
              "name": "onCreateOptionsMenu",
              "count": 2,
              "maxNeededTime": 4,
              "overallNeededTime": 6,
              "averageNeededTime": "3.00"
            },
            {
              "name": "onOptionsItemSelected",
              "count": 2,
              "maxNeededTime": 16,
              "overallNeededTime": 32,
              "averageNeededTime": "16.00"
            }
          ]
        },
        {
          "name": "MainActivity",
          "children": [
            {
              "name": "onCreate",
              "count": 1,
              "maxNeededTime": 188,
              "overallNeededTime": 188,
              "averageNeededTime": "188.00"
            }
          ]
        },
        {
          "name": "PagerAdapter",
          "children": [
            {
              "name": "getItem",
              "count": 1,
              "maxNeededTime": 1,
              "overallNeededTime": 1,
              "averageNeededTime": "1.00"
            }
          ]
        },
        {
          "name": "PathHelper",
          "children": [
            {
              "name": "getAppDataRootPath",
              "count": 1,
              "maxNeededTime": 9,
              "overallNeededTime": 9,
              "averageNeededTime": "9.00"
            }
          ]
        },
        {
          "name": "ResizableImageView",
          "children": [
            {
              "name": "onMeasure",
              "count": 106,
              "maxNeededTime": 10,
              "overallNeededTime": 143,
              "averageNeededTime": "1.35"
            },
            {
              "name": "scaleImage",
              "count": 84,
              "maxNeededTime": 10,
              "overallNeededTime": 111,
              "averageNeededTime": "1.32"
            }
          ]
        },
        {
          "name": "SaveBitmapToFavorites",
          "children": [
            {
              "name": "SaveBitmapToFavorite",
              "count": 3,
              "maxNeededTime": 267,
              "overallNeededTime": 762,
              "averageNeededTime": "254.00"
            }
          ]
        },
        {
          "name": "SearchFragment",
          "children": [
            {
              "name": "onActivityCreated",
              "count": 1,
              "maxNeededTime": 1,
              "overallNeededTime": 1,
              "averageNeededTime": "1.00"
            },
            {
              "name": "onClick",
              "count": 2,
              "maxNeededTime": 7,
              "overallNeededTime": 11,
              "averageNeededTime": "5.50"
            },
            {
              "name": "onCreateView",
              "count": 1,
              "maxNeededTime": 22,
              "overallNeededTime": 22,
              "averageNeededTime": "22.00"
            },
            {
              "name": "onImageListDownloaded",
              "count": 1,
              "maxNeededTime": 1,
              "overallNeededTime": 1,
              "averageNeededTime": "1.00"
            },
            {
              "name": "searchStart",
              "count": 2,
              "maxNeededTime": 7,
              "overallNeededTime": 11,
              "averageNeededTime": "5.50"
            }
          ]
        },
        {
          "name": "SearchRecyclerViewAdapter",
          "children": [
            {
              "name": "onBindViewHolder",
              "count": 32,
              "maxNeededTime": 6,
              "overallNeededTime": 42,
              "averageNeededTime": "1.31"
            },
            {
              "name": "onCreateViewHolder",
              "count": 25,
              "maxNeededTime": 9,
              "overallNeededTime": 65,
              "averageNeededTime": "2.60"
            }
          ]
        },
        {
          "name": "SquareCardView",
          "children": [
            {
              "name": "onMeasure",
              "count": 94,
              "maxNeededTime": 11,
              "overallNeededTime": 214,
              "averageNeededTime": "2.28"
            }
          ]
        },
        {
          "name": "ThumbnailsAdapter",
          "children": [
            {
              "name": "onCreateViewHolder",
              "count": 3,
              "maxNeededTime": 1,
              "overallNeededTime": 3,
              "averageNeededTime": "1.00"
            }
          ]
        },
        {
          "name": "ThumbnailsManager",
          "children": [
            {
              "name": "processThumbs",
              "count": 1,
              "maxNeededTime": 63,
              "overallNeededTime": 63,
              "averageNeededTime": "63.00"
            }
          ]
        }
      ]
    },
    {
      "name": "OtherThreads",
      "children": [
        {
          "name": "AddSQLTask",
          "children": [
            {
              "name": "doInBackground",
              "count": 6,
              "maxNeededTime": 12,
              "overallNeededTime": 61,
              "averageNeededTime": "10.17"
            }
          ]
        },
        {
          "name": "FlickrTask",
          "children": [
            {
              "name": "doInBackground",
              "count": 2,
              "maxNeededTime": 922,
              "overallNeededTime": 1698,
              "averageNeededTime": "849.00"
            },
            {
              "name": "getJson",
              "count": 2,
              "maxNeededTime": 772,
              "overallNeededTime": 1454,
              "averageNeededTime": "727.00"
            }
          ]
        },
        {
          "name": "GettyImagesTask",
          "children": [
            {
              "name": "doInBackground",
              "count": 1,
              "maxNeededTime": 1032,
              "overallNeededTime": 1032,
              "averageNeededTime": "1032.00"
            },
            {
              "name": "getJson",
              "count": 1,
              "maxNeededTime": 914,
              "overallNeededTime": 914,
              "averageNeededTime": "914.00"
            }
          ]
        },
        {
          "name": "ImageSaveTask",
          "children": [
            {
              "name": "doInBackground",
              "count": 3,
              "maxNeededTime": 153,
              "overallNeededTime": 272,
              "averageNeededTime": "90.67"
            }
          ]
        }
      ]
    }
  ]
}